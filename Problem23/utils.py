from Problem12.utils import divisor_generator


def get_abundant_sums(n):
    abundant_sums = []
    for i in range(1, n):
        sum_of_divisors = sum(divisor_generator(i)) - i
        if sum_of_divisors > i:
            abundant_sums.append(i)

    return abundant_sums



def get_set_of_numbers_sum_of_two_abundants(abundants):
    sum_abundants = set()

    for i in abundants:
        for o in abundants:
            sum_abundants.add(i+o)

    return sum_abundants




