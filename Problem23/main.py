from Problem23.utils import get_abundant_sums, get_set_of_numbers_sum_of_two_abundants

abundants = get_abundant_sums(28123)
sum_of_two_abundants = get_set_of_numbers_sum_of_two_abundants(abundants)

print sum(i for i in range(28123) if i not in sum_of_two_abundants)




