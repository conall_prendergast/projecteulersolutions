import unittest
from Problem5.utils import *


class TestProblem5(unittest.TestCase):
    def test_is_divisible(self):
        assert is_divisible(4, 2)
        assert is_divisible(8, 4)
        assert is_divisible(100, 5)
        assert is_divisible(100, 20)
        assert not is_divisible(100, 3)

    def test_is_num_divisible_up_to_10(self):
        assert is_num_divisible_by_nums_up_to(2520, 10)
        assert not is_num_divisible_by_nums_up_to(2520, 20)


if __name__ == '__main__':
    unittest.main()
