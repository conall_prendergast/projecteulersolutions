def is_divisible(n, m):
    """
    Returns true if n is evenly divisible by m
    :param n: the numerator
    :param m: the denominator
    :return: True if m goes into n evenly
    """
    return n % m == 0


def is_num_divisible_by_nums_up_to(num, divisible_up_to):
    """
    Returns True if "num" is evenly divisible by all the integers up to "divisible_up_to"
    :param num: numerator
    :param divisible_up_to: the limit for the denominator loop
    :return: True if "num" is evenly divisible by all the integers up to "divisible_up_to" inclusively
    """

    for i in range(1, divisible_up_to+1):
        if not is_divisible(num, i):
            return False
    return True



