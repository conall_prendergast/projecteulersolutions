class Node:

    def __init__(self, value):
        self.value = int(value)
        self.children = []


    def add_child(self, child):
        self.children.append(child)

    def get_max_path_from_this_node(self):
        if not self.children:
            return self.value
        else:
            return self.value + max(self.children[0].get_max_path_from_this_node(),
                                    self.children[1].get_max_path_from_this_node())