from Problem17.Node import Node


def read_file_into_list():
    with open("pyramid") as fp:
        return fp.read().splitlines()


def read_list_into_node(list):
    """
    Read a list of node value strings into a list of nodes.
    Attach each node to its child and return the root
    :param list: list of node vals eg. eg .['75', '95 64', '17 47 82']
    :return: the root node with child nodes attached
    """
    Nodes = []

    # Create the nodes
    for row_num, row in enumerate(list):
        Nodes.append([])
        for node_num, node_val in enumerate(row.split(" ")):
            Nodes[row_num].append(Node(node_val))



    # Attach the nodes
    for row_num in range(0, len(Nodes) -1):
        for node_num, node in enumerate(Nodes[row_num]):
            node.add_child(Nodes[row_num+1][node_num])
            node.add_child(Nodes[row_num + 1][node_num + 1])

    # Return the root node
    return Nodes[0][0]

