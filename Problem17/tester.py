import unittest
from Problem17.utils import read_file_into_list, read_list_into_node
from Problem17.Node import Node

class TestProblem17(unittest.TestCase):


    def test_read_file(self):
        print read_file_into_list()


    def test_read_array_into_nodes(self):
        list_of_node_vals = read_file_into_list()
        root = read_list_into_node(list_of_node_vals)


    def test_get_max_val_from_root_node(self):
        list_of_node_vals = read_file_into_list()
        root = read_list_into_node(list_of_node_vals)
        print root.get_max_path_from_this_node()

if __name__ == '__main__':
    unittest.main()
