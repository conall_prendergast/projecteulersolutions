import string
alphabet = string.ascii_uppercase



with open("names.txt") as fp:
    raw_file = fp.read()

names = [text[1:-1] for text in raw_file.split(",")]
names.sort()

total = 0
for position, name in enumerate(names):
    total += sum(alphabet.index(letter) + 1 for letter in name) * (position + 1)




print total