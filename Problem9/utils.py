

def gen_pythagorean_triplet(max_len):
    """
    Generated pythagorean triplets with the max length of any side specified
    :param max_len: max length of a side
    """

    for a in range(1, max_len + 1):
        for b in range(a, max_len + 1):
            c = ((a * a) + (b * b)) ** 0.5
            if c % 1 == 0:
                yield a, b, int(c)
