from Problem9.utils import *
import unittest


class TestProblem9(unittest.TestCase):
    def test_pythagorean_trips(self):
        assert (3, 4, 5) in list(gen_pythagorean_triplet(10))


if __name__ == '__main__':
    unittest.main()
