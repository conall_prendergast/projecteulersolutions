from utils import gen_pythagorean_triplet

trips = gen_pythagorean_triplet(1000)

while True:
    a, b, c = trips.next()

    if a + b + c == 1000:
        print a, b, c, a*b*c
        break

