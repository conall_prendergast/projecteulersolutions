import unittest

from Problem11.utils import get_product_of_n_numbers_across, \
    get_product_of_n_numbers_downleft, get_product_of_n_numbers_downright, \
    get_product_of_n_numbers_downwards


class TestProblem12(unittest.TestCase):
    gridString = "08 02 22\n\
49 49 99\n\
81 49 31"

    grid = [line.split(" ") for line in gridString.split("\n")]
    testPoint = (0, 0)

    def test_product_downwards(self):
        assert get_product_of_n_numbers_downwards(self.grid, self.testPoint, 3) == 31752

    def test_product_across(self):
        assert get_product_of_n_numbers_across(self.grid, self.testPoint, 3) == 352

    def test_product_downleft(self):
        assert get_product_of_n_numbers_downleft(self.grid, (0, 2), 3) == 87318

    def test_product_downright(self):
        assert get_product_of_n_numbers_downright(self.grid, self.testPoint, 3) == 12152


if __name__ == '__main__':
    unittest.main()
