def get_product_of_n_numbers_downwards(grid, (x, y), n):
    return reduce(_mult, [grid[x + i][y] for i in range(n)])


def get_product_of_n_numbers_across(grid, (x, y), n):
    return reduce(_mult, [grid[x][y + i] for i in range(n)])


def get_product_of_n_numbers_downleft(grid, (x, y), n):
    return reduce(_mult, [grid[x + i][y - i] for i in range(n)])


def get_product_of_n_numbers_downright(grid, (x, y), n):
    return reduce(_mult, [grid[x + i][y + i] for i in range(n)])


def _mult(a, b):
    return int(a) * int(b)
