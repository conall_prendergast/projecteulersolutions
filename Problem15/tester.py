import unittest
from Problem15.utils import get_num_paths_from_location

class TestProblem15(unittest.TestCase):
    def test_get_num_paths_2x2(self):
        assert get_num_paths_from_location((2,2)) == 6


if __name__ == '__main__':
    unittest.main()
