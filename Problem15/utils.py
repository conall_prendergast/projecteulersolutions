path_lengths = {}


def remember_num_paths(func):
    def wrapper(start):
        if start not in path_lengths:
            path_lengths[start] = func(start)
        return path_lengths[start]

    return wrapper


@remember_num_paths
def get_num_paths_from_location(point):
    if point == (0, 0):
        return 1
    else:
        num_paths = 0
        if point[0] != 0:
            num_paths += get_num_paths_from_location((point[0] - 1, point[1]))

        if point[1] != 0:
            num_paths += get_num_paths_from_location((point[0], point[1] - 1))

        return num_paths
