import utils


sieve = [True] * 1000
utils.mark_sieve_with_multiples(sieve, False, 3, 5)
print utils.get_sum_of_indices_equal_to(sieve, False)
