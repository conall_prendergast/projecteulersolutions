

def mark_sieve_with_multiples(sieve, marker,  *mults):
    """
    Sets every nth index of sieve to false
    :param marker: sets each "nth" element to "marker"
    :param sieve: Sets every nth index of sieve to false
    :param mults: the multiples which need to be marked as false. list of "n's"
    :type sieve: list
    :type mults: list
    """
    for mult in mults:
        if sieve[mult] == marker:
            continue
        for i in range(mult, len(sieve), mult):
            sieve[i] = marker


def get_sum_of_indices_equal_to(sieve, equals):
    """
    Returns a summation of all the indices in a list which are equal to "equals"
    :param sieve: list of True/False values
    :param equals: value to compare list elements to
    :type sieve: list
    :return: sum of true values
    """
    return sum(index for index, value in enumerate(sieve) if value == equals)






