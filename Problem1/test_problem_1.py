import unittest
from Problem1.utils import mark_sieve_with_multiples, get_sum_of_indices_equal_to
from mock import MagicMock


class TestProblem1(unittest.TestCase):

    def test_mark_sieve_with_multiples(self):
        sieve = [True] * 10
        mark_sieve_with_multiples(sieve, False, 2)
        for i in range(2, len(sieve), 2):
            assert sieve[i] == False

    def test_mark_sieve_sieve_index_marked_continue(self):
        """
        Asserts that is the sieve multiple index is already false, than the loop
        does not bother to mark subsequent multiples of said index
        eg. is the loop is to mark multiples of 4 as false, and 4 is already false, then every multiple
        of 4 is also already marked (by 2 maybe)
        """

        sieve = MagicMock()
        sieve.__len__ = MagicMock(return_value=10)
        sieve.__getitem__ = MagicMock(return_value=False)
        mark_sieve_with_multiples(sieve, False, 2)
        assert len(sieve.mock_calls) == 1

    def test_summation_function(self):
        sieve = [True, True, False, False, True, True]
        assert get_sum_of_indices_equal_to(sieve, False) == 5
        sieve = [True] * 10
        mark_sieve_with_multiples(sieve, False, 3, 5)
        assert get_sum_of_indices_equal_to(sieve, False) == 23


if __name__ == '__main__':
    unittest.main()
