from itertools import permutations
perms = list(permutations([0,1,2,3,4,5,6,7,8,9]))[1000000]


print "".join(str(perm) for perm in perms)