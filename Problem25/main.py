from Problem2.utils import generate_fibonaccis


fibs = generate_fibonaccis()

index = 0
fibs.next()
while True:
    index += 1
    fib = fibs.next()
    if(len(str(fib))) == 1000:
        print index
        break