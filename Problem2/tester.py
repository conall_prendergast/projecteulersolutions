import unittest
from Problem2 import utils
from itertools import islice


class TestProblem2(unittest.TestCase):
    def test_fibonacci_sequence(self):
        fibs = utils.generate_fibonaccis()
        first_ten_fibs = islice(fibs, 0, 10)
        assert list(first_ten_fibs) == [0, 1, 1, 2, 3, 5, 8, 13, 21, 34]


if __name__ == '__main__':
    unittest.main()
