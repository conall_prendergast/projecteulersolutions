import utils
from itertools import takewhile

fibs = utils.generate_fibonaccis()


print sum(i for i in takewhile(lambda x: x < 4000000, fibs) if i % 2 == 0)
