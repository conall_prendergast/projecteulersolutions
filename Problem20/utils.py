def get_sum_of_numbers(n):
    """
    Gets the sum of all the individual digits in a number
    :param n: the number of which to get the sum of its digits
    :type n: int
    :rtype: int
    :return: the sum of the digits
    """
    return sum(int(i) for i in str(n))