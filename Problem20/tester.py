import unittest
from Problem20.utils import get_sum_of_numbers


class TestProblem20(unittest.TestCase):
    def test_get_sum_of_numbers(self):
        assert get_sum_of_numbers(3628800) == 27




if __name__ == '__main__':
    unittest.main()
