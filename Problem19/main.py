from datetime import date, timedelta

first_sunday = date(1901, 1, 6)

num_of_sundays_on_first = 0

next_sunday = first_sunday + timedelta(days=7)
while(next_sunday < date(2001, 1, 1)):

   next_sunday += timedelta(days=7)
   if next_sunday.day == 1:
       num_of_sundays_on_first += 1

print num_of_sundays_on_first
