from Problem14.utils import len_of_collatz
longest_chain = 0, 0

for i in range(1, 1000000):
    if len_of_collatz(i) > longest_chain[1]:
        longest_chain = i, len_of_collatz(i)

print longest_chain

