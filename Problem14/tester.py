import unittest
from Problem14.utils import len_of_collatz


class TestProblem14(unittest.TestCase):
    def test_generate_collatz(self):
        self.assertEqual(len_of_collatz(13), 10)


if __name__ == '__main__':
    unittest.main()
