seq_lengths = {}


def remember_seq_len(func):
    def wrapper(start):
        if start not in seq_lengths:
            seq_lengths[start] = func(start)
        return seq_lengths[start]
    return wrapper


@remember_seq_len
def len_of_collatz(start):
    if start == 1:
        return 1
    elif start % 2 == 0:
        return 1 + len_of_collatz(start / 2)
    else:
        return 1 + len_of_collatz(3*start + 1)







