
def get_primes_up_to(n):
    """
    Sieve of eratosthenes; Gets all the primes up to a certain number
    :param n: gets primes up to this number
    :return: list of primes
    """
    sieve = [True] * n
    sieve[0] = False
    sieve[1] = False

    for mult in range(2, n):
        for i in range(mult*2, len(sieve), mult):
            sieve[i] = False

    return list(key for key, value in enumerate(sieve) if value)

