import utils

primes = utils.get_primes_up_to(100000)
n = 600851475143

print max(prime for prime in primes if n % prime == 0)
