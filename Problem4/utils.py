
def is_string_palindromic(param):
    if len(param) < 2:
        return True
    elif param[0] == param[-1]:
        return is_string_palindromic(param[1:-1])
    return False


def is_number_palindromic(n):
    """
    Returns true if number is palindromic

    :type n: int
    :return: True if n is palindromic
    :rtype: bool
    """
    return is_string_palindromic(str(n))







