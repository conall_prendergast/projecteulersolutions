import unittest
from Problem4 import utils
from mock import patch, MagicMock


class TestProblem4(unittest.TestCase):

    @patch("Problem4.utils.is_string_palindromic", return_value=True)
    def test_is_number_palindromic_str_func_called(self, mock_is_str_palindromic):
        """
        :type mock_is_str_palindromic: MagicMock
        """

        utils.is_number_palindromic(4)
        assert mock_is_str_palindromic.called

    def test_sanity_check_obvious_numbers(self):
        assert utils.is_number_palindromic(4004)
        assert utils.is_number_palindromic(12321)
        assert not utils.is_number_palindromic(4005)
        assert not utils.is_number_palindromic(4014)
        assert not utils.is_number_palindromic(6004)

if __name__ == '__main__':
    unittest.main()
