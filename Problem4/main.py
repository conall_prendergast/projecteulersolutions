import utils

largest_pal = 0, 0, 0

for i in range(100, 1000):
    for o in range(i, 1000):
        if utils.is_number_palindromic(i * o) and i * o > largest_pal[2]:
            largest_pal = i, o, i * o

print largest_pal
