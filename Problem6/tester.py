from Problem6.utils import *
import unittest


class TestProblem6(unittest.TestCase):
    def test_square_of_sums(self):
        assert square_of_sum_of_first(10) == 3025

    def test_sum_of_squares(self):
        assert sum_of_squares_of_first(10) == 385

    def test_square_sum_sum_square_difference(self):
        assert get_square_sum_sum_square_difference(10) == 2640


if __name__ == '__main__':
    unittest.main()
