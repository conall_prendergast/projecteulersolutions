

def sum_of_squares_of_first(n):
    return sum(i*i for i in range(1, n+1))


def square_of_sum_of_first(n):
    return sum(i for i in range(1, n+1)) ** 2


def get_square_sum_sum_square_difference(n):
    return square_of_sum_of_first(n) - sum_of_squares_of_first(n)

