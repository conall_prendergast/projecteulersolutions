import math


def divisor_generator(n):
    """
    Generates divisors of a number
    http://stackoverflow.com/questions/171765/what-is-the-best-way-to-get-all-the-divisors-of-a-number
    :param n: number of which to get divisors
    :return: Generator of divisors
    """
    large_divisors = []
    for i in xrange(1, int(math.sqrt(n) + 1)):
        if n % i == 0:
            yield i
            if i*i != n:
                large_divisors.append(n / i)
    for divisor in reversed(large_divisors):
        yield divisor


def get_num_divisors(n):
    """
    Returns the number of divisors from a number
    :param n: the number of which to get the number
    :return: number of divisors
    """
    return len(list(divisor_generator(n)))


def get_triangle_numbers():
    """
    Generates triangle numbers, starting from 1, stretching to infinity
    :return: Generator of triangle numbers
    """
    triangle_number = 0
    adder = 1

    while True:
        triangle_number += adder
        adder += 1
        yield triangle_number
