from Problem12.utils import get_triangle_numbers, get_num_divisors

triangle_nums = get_triangle_numbers()

while True:
    num = triangle_nums.next()

    if get_num_divisors(num) > 500:
        print num
        break
