import unittest
from Problem12.utils import get_num_divisors, get_triangle_numbers
from itertools import islice


class TestProblem12(unittest.TestCase):
    def test_getNumDivisiors(self):
        print get_num_divisors(1)
        assert get_num_divisors(1) == 1
        assert get_num_divisors(3) == 2
        assert get_num_divisors(6) == 4
        assert get_num_divisors(10) == 4
        assert get_num_divisors(15) == 4
        assert get_num_divisors(21) == 4
        assert get_num_divisors(28) == 6
        assert get_num_divisors(76576500) > 500

    def test_gen_triangle_number(self):
        assert list(islice(get_triangle_numbers(), 0, 7)) == [1, 3, 6, 10, 15, 21, 28]


if __name__ == '__main__':
    unittest.main()
