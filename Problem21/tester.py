import unittest
from Problem21.utils import is_amicable

class TestProblem21(unittest.TestCase):
    def test_is_amicable(self):
        assert is_amicable(220)
        assert is_amicable(284)

    def test_is_not_amicable(self):
        assert not is_amicable(10)


if __name__ == '__main__':
    unittest.main()
