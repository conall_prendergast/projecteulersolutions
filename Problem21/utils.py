from Problem12.utils import divisor_generator

sum_of_divisors = {}

def is_amicable(n):
    """
    Returns true if n is anicale
    :param n: number to check if is amicable
    :return: True if n is amicable
    """
    # Sum of n's divisors (dont count the number itself)
    a = sum(divisor_generator(n)) - n

    # sum of sum's divisors
    b = sum(divisor_generator(a)) - a
    
    if a == b:
        return False

    if b == n:
        return True
    return False
