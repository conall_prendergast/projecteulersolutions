from Problem21.utils import is_amicable


print sum(i for i in range(1, 10000) if is_amicable(i))